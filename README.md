# IoT Aggregator

## Configuration

### Integration service
- S3__ExternalAddress
Url which will be concatenated to *{bucketName}/{path}* when querying photos. Use *{protocol}://{domain}/api/img* if users should retrieve images through **Integration service** API.
- RUN_SAMPLE_WORKER 
*"1"* if worker publishing sample measurements should run, otherwise don't set any values.
### Prometheus
* EXTERNAL_URL
Url under which **Prometheus** is accessible to client (e.g. http://localhost/prometheus).
### Grafana
* GF_SERVER_ROOT_URL
Url under which **Grafana** is accessible to client (e.g. http://localhost/grafana).
* GF_SERVER_PROTOCOL
Protocol under which **Grafana** is accessible to client (http/https).
* GF_SERVER_DOMAIN
Domain under which **Grafana** is accessible to client.
### Web
* GRAFANA_URL
Path under which **Grafana** is accessible to client.
* INTEGRATION_URL
Path under which **Integration service** is accessible to client.
* BUCKET_NAME
Bucket name used for MinIO configuration with `configure-bucket.sh` script .
### Nginx configuration
* Set `server_name` for second http server (used for `configure-bucket.sh` script and by raspberry device to push images).